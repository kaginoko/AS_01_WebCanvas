# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---

## **Right Menu (Top to bottom)**  
### **Pen Tool**
You can click and drag the pen to draw on the canvas.

### **Eraser Tool**
You can click and drag the eraser tool to erase from the canvas
### **Shapes Tool**
You can click and drag to creat four different shapes on the canvas. (Triangle, Circle, Rectangle, and star shape)
### **Text Tool**
After clicking in this option, you can select the top option to start adding text to the canvas or edit the style of your text. Type in the dotted lines, click on where you want to place the text and press the check button to apply to the canvas.  
The bottom option is for changing between fonts.

### **Image Options**
You can upload your own images to edit or download your creations.
### **Undo/Redo**
You can undo/redo an action that affected the canvas, it only works up to 10 times. There are counters indicating how many steps you can go back/forward.
### **Clear All**
This clears the canvas, you can use the undo button to undo this action.

---
## **Left Menu (Top to bottom)**
### **Color Selector**
You can select the hue of the color from the circle and the brightness of the color using the slider option. The outer part of the menu show you the color you have currently selected.
### **Pen Size Slider**
This changes the size of the line, it shows you a preview of how big the line will be. This affects the Pen, the Eraser and all shapes.
### **Pen Opacity Slider**
This changes the opacity of the line, it shows you a preview of how opaque the line will be. This affects the Pen, the Eraser, all shapes and the text tool.

---

## Additional Properties
### Draggable Menu
You can drag the menus around the screen by dragging the top, lighter part of the menu.

## Gitlab Link
[This tool is hosted on Gitlab](https://108071050.gitlab.io/AS_01_WebCanvas/)


## Others (Optional)
My additions are: Star Shape tool, Bold/Italic Font options, Draggable Menu, Counter for number of Undo/Redos.